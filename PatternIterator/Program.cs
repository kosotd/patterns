﻿using System;
using PatternIterator.Aggregate.Abstract;
using PatternIterator.Aggregate.Concrete;
using PatternIterator.Iterator.Abstract;

namespace PatternIterator
{
    class Program
    {
        static void Main(string[] args)
        {
            const int N = 6;
            const int L = 0;
            const int R = 100;

            ICollection<int> collection = new SimpleList<int>();

            Random rnd = new Random();
            for (int i = 0; i < N; ++i)
                collection.Append(rnd.Next(L, R));

            IIterator<int> iterator = collection.CreateIterator();
            for (iterator.First(); !iterator.IsDone(); iterator.Next())
                Console.Write("{0} ", iterator.CurrentItem());

            Console.ReadKey();
        }
    }
}

﻿using System.Threading;
using PatternIterator.Iterator.Abstract;

namespace PatternIterator.Aggregate.Abstract
{
    interface ICollection<T>
    {
        IIterator<T> CreateIterator();
        int Count();
        void Append(T item);
        void Remove(T item);
    }
}

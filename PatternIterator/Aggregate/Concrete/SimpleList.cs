﻿using System;
using PatternIterator.Aggregate.Abstract;
using PatternIterator.Iterator.Abstract;
using PatternIterator.Iterator.Concrete;

namespace PatternIterator.Aggregate.Concrete
{
    class SimpleList<T> : ICollection<T> where T : IComparable
    {
        private int size;
        private T[] data;
        private int currPos;

        public SimpleList()
        {
            currPos = -1;
            size = 10;
            data = new T[size];
        }

        private void Extend()
        {
            size *= 2;
            T[] newData = new T[size];
            for (int i = 0; i < currPos; ++i)
                newData[i] = data[i];
            data = newData;
        }
        public IIterator<T> CreateIterator()
        {
            return new SimpleListIterator<T>(data, currPos);
        }

        public int Count()
        {
            return currPos + 1;
        }

        public void Append(T item)
        {
            if (++currPos >= size)
                Extend();
            data[currPos] = item;
        }

        private int Find(T item)
        {
            for (int i = 0; i <= currPos; ++i)
                if (item.Equals(data[i]))
                    return i;
            return -1;
        }
        public void Remove(T item)
        {
            int ind = Find(item);
            if (ind > -1)
            {
                for (int i = ind; i <= currPos; ++i)
                    data[i] = data[i + 1];
                --currPos;
            }
        }
    }
}

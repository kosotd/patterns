﻿using System;
using PatternIterator.Iterator.Abstract;

namespace PatternIterator.Iterator.Concrete
{
    class SimpleListIterator<T> : IIterator<T>
    {
        private int size;
        private int currPos;
        private T[] data;
        public SimpleListIterator(T[] data, int size)
        {
            this.data = data;
            this.size = size;
        } 
        public void First()
        {
            currPos = 0;
        }

        public void Next()
        {
            ++currPos;
        }

        public bool IsDone()
        {
            return (currPos > size);
        }

        public T CurrentItem()
        {
            if (currPos < 0 || currPos > size)
                throw new IndexOutOfRangeException();
            return data[currPos];
        }
    }
}

﻿namespace PatternIterator.Iterator.Abstract
{
    interface IIterator<out T>
    {
        void First();
        void Next();
        bool IsDone();
        T CurrentItem();
    }
}

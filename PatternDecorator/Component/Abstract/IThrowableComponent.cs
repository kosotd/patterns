﻿namespace PatternDecorator.Component.Abstract
{
    interface IThrowableComponent
    {
        void Operation();
    }
}

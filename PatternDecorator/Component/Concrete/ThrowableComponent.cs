﻿using System;
using PatternDecorator.Component.Abstract;

namespace PatternDecorator.Component.Concrete
{
    class ThrowableComponent : IThrowableComponent
    {
        private string message;
        public ThrowableComponent(string message)
        {
            this.message = message;
        }
        public void Operation()
        {
            throw new NotImplementedException(message);
        }
    }
}

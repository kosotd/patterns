﻿using PatternDecorator.Component.Abstract;

namespace PatternDecorator.Decorator.Abstract
{
    abstract class Decorator : IThrowableComponent
    {
        private IThrowableComponent throwableComponent;
        protected Decorator(IThrowableComponent throwableComponent)
        {
            this.throwableComponent = throwableComponent;
        }
        public virtual void Operation()
        {
            throwableComponent.Operation();
        }
    }
}

﻿using System;
using System.IO;
using PatternDecorator.Component.Abstract;

namespace PatternDecorator.Decorator.Concrete
{
    class FileLogDecorator : Abstract.Decorator
    {
        private StreamWriter streamWriter;
        public FileLogDecorator(IThrowableComponent throwableComponent, string logFileName) : base(throwableComponent)
        {
            streamWriter = new StreamWriter(logFileName);
        }

        public override void Operation()
        {
            try
            {
                base.Operation();
            }
            catch (Exception e)
            {
                streamWriter.Write(e.Message);
                streamWriter.Close();
            }
        }
    }
}

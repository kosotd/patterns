﻿using System;
using PatternDecorator.Component.Abstract;

namespace PatternDecorator.Decorator.Concrete
{
    class ConsoleLogDecorator : Abstract.Decorator
    {
        public ConsoleLogDecorator(IThrowableComponent throwableComponent) : base(throwableComponent) { }

        public override void Operation()
        {
            try
            {
                base.Operation();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}

﻿using System;
using PatternDecorator.Component.Abstract;
using PatternDecorator.Component.Concrete;
using PatternDecorator.Decorator.Concrete;

namespace PatternDecorator
{
    class Program
    {
        static void Main(string[] args)
        {
            IThrowableComponent throwableComponent = new FileLogDecorator(new ThrowableComponent("FileLogDecorator error"), "output.txt");
            throwableComponent.Operation();

            throwableComponent = new ConsoleLogDecorator(new ThrowableComponent("ConsoleLogDecorator error"));
            throwableComponent.Operation();

            Console.ReadKey();
        }
    }
}

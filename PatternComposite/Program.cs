﻿using System;
using PatternComposite.Composite.Concrete;

namespace PatternComposite
{
    class Program
    {
        static void Main(string[] args)
        {
            Text text = new Text();
            Row row1 = new Row("row 1 text");
            row1.Add(new Alpha('g'));
            row1.Add(new Alpha('l'));
            row1.Add(new Alpha('y'));
            row1.Add(new Alpha('p'));
            row1.Add(new Alpha('h'));
            row1.Add(new Alpha('s'));
            Row row2 = new Row("row 2 text");
            row2.Add(new Number(1233411));
            Row row3 = new Row("row 3 text");
            text.Add(row1);
            text.Add(row2);
            text.Add(row3);
            text.Write();
            Console.ReadKey();
        }
    }
}

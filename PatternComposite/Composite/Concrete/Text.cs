﻿using System;
using System.Collections.Generic;
using PatternComposite.Composite.Abstract;

namespace PatternComposite.Composite.Concrete
{
    class Text : Glyph
    {
        public override void Write()
        {
            foreach (var glyph in childs)
            {
                glyph.Write();
                Console.WriteLine();
            }
        }
    }
}

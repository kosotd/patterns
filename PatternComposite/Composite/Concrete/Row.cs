﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternComposite.Composite.Abstract;

namespace PatternComposite.Composite.Concrete
{
    class Row : Glyph
    {
        private string data;
        public Row(string str)
        {
            data = str;
        }
        public override void Write()
        {
            Console.Write(data);
            foreach (var glyph in childs)
                glyph.Write();
        }
    }
}

﻿using System;
using PatternComposite.Composite.Abstract;

namespace PatternComposite.Composite.Concrete
{
    class Number : Glyph
    {
        private int data;

        public Number(int number)
        {
            data = number;
        }
        public override void Write()
        {
            Console.Write(data);
        }

        public override void Add(Glyph glyph)
        {
            throw new NotImplementedException("Impossible add child to number");
        }

        public override void Remove(Glyph glyph)
        {
            throw new NotImplementedException("Number has not child");
        }

        public override Glyph GetChild(int num)
        {
            throw new NotImplementedException("Number has not child");
        }
    }
}

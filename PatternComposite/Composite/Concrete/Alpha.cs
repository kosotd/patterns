﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PatternComposite.Composite.Abstract;

namespace PatternComposite.Composite.Concrete
{
    class Alpha : Glyph
    {
        private char data;
        public Alpha(char symbol)
        {
            data = symbol;
        }
        public override void Write()
        {
            Console.Write(data);
        }

        public override void Add(Glyph glyph)
        {
            throw new NotImplementedException("Impossible add child to alpha");
        }

        public override void Remove(Glyph glyph)
        {
            throw new NotImplementedException("Alpha has not child");
        }

        public override Glyph GetChild(int num)
        {
            throw new NotImplementedException("Alpha has not child");
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace PatternComposite.Composite.Abstract
{
    abstract class Glyph
    {
        protected List<Glyph> childs = new List<Glyph>();
        public abstract void Write();

        public virtual void Add(Glyph glyph)
        {
            childs.Add(glyph);
        }

        public virtual void Remove(Glyph glyph)
        {
            childs.Remove(glyph);
        }

        public virtual Glyph GetChild(int num)
        {
            if (num < 0 || num > childs.Count)
                throw new IndexOutOfRangeException();
            return childs[num];
        }
    }
}

﻿using System;
using PatternStrategy.PrintStrategy.Abstract;

namespace PatternStrategy.PrintStrategy.Concrete
{
    class ConsolePrinter : IPrintStrategy
    {
        public void BeginWrite()
        {
        }

        public void EndWrite()
        {
        }

        public void Write(string s)
        {
            Console.Write(s);
        }

        public void WriteLine(string s)
        {
            Console.WriteLine(s);
        }
    }
}

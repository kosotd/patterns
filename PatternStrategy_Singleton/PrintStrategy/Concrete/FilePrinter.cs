﻿using System.IO;
using PatternStrategy.PrintStrategy.Abstract;

namespace PatternStrategy.PrintStrategy.Concrete
{
    class FilePrinter : IPrintStrategy
    {
        private StreamWriter streamWriter;
        public FilePrinter(string fileName)
        {
            streamWriter = new StreamWriter(fileName);
        }

        public void BeginWrite()
        {
        }

        public void EndWrite()
        {
            streamWriter.Close();
        }

        public void Write(string s)
        {
            streamWriter.Write(s);
        }
        public void WriteLine(string s)
        {
            streamWriter.WriteLine(s);
        }
    }
}

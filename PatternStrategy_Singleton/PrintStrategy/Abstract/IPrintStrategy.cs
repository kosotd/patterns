﻿namespace PatternStrategy.PrintStrategy.Abstract
{
    interface IPrintStrategy
    {
        void BeginWrite();
        void EndWrite();
        void Write(string s); 
        void WriteLine(string s);
    }
}

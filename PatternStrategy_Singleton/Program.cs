﻿using System;
using PatternStrategy.Context;
using PatternStrategy.Factory;
using PatternStrategy.PrintStrategy;
using PatternStrategy.PrintStrategy.Abstract;
using PatternStrategy.SortStrategy;
using PatternStrategy.SortStrategy.Abstract;

namespace PatternStrategy
{
    class Program
    {
        static void Main(string[] args)
        {
            const int N = 20;
            const int L = 0;
            const int R = 100;

            ISortStrategy<int> sortStrategy = StrategysRegistry.Instance.GetSortStrategy<int>();
            IPrintStrategy printStrategy = StrategysRegistry.Instance.GetPrintStrategy();

            Collection<int> collection = new Collection<int>(sortStrategy, printStrategy);

            Random rnd = new Random();
            for (int i = 0; i < N; ++i)
                collection.Add(rnd.Next(L, R));

            collection.Sort(true);
            collection.Print();

            Console.ReadKey();
        }
    }
}

﻿using System;
using PatternStrategy.SortStrategy.Abstract;

namespace PatternStrategy.SortStrategy.Concrete
{
    class QuickSort<T> : ISortStrategy<T> where T : IComparable
    {
        private void InternalSort(T[] a, int l, int r, int comp)
        {
            T temp;
            T x = a[l + (r - l) / 2];
            int i = l;
            int j = r;
            while (i <= j)
            {
                while (a[i].CompareTo(x) == -comp) i++;
                while (a[j].CompareTo(x) == comp) j--;
                if (i <= j)
                {
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                    i++;
                    j--;
                }
            }
            if (i < r)
                InternalSort(a, i, r, comp);

            if (l < j)
                InternalSort(a, l, j, comp);
        }
        public void Sort(T[] collection, bool reverse = false)
        {
            int comp = 1;
            if (reverse)
                comp = -1;
            InternalSort(collection, 0, collection.Length - 1, comp);
        }
    }
}

﻿using System;
using PatternStrategy.SortStrategy.Abstract;

namespace PatternStrategy.SortStrategy.Concrete
{
    class StupidSort<T> : ISortStrategy<T> where T : IComparable
    {
        public void Sort(T[] collection, bool reverse = false)
        {
            int comp = 1;
            if (reverse)
                comp = -1;
            for (int i = 0; i < collection.Length - 1; ++i)
                for (int j = i + 1; j < collection.Length; ++j)
                    if (collection[i].CompareTo(collection[j]) == comp)
                    {
                        T tmp = collection[i];
                        collection[i] = collection[j];
                        collection[j] = tmp;
                    }
        }
    }
}

﻿using System;

namespace PatternStrategy.SortStrategy.Abstract
{
    interface ISortStrategy<in T> where T : IComparable
    {
        void Sort(T[] collection, bool reverse = false);
    }
}

﻿using System;
using System.Configuration;
using PatternStrategy.PrintStrategy;
using PatternStrategy.PrintStrategy.Abstract;
using PatternStrategy.PrintStrategy.Concrete;
using PatternStrategy.SortStrategy;
using PatternStrategy.SortStrategy.Abstract;
using PatternStrategy.SortStrategy.Concrete;

namespace PatternStrategy.Factory
{
    class StrategysRegistry
    {
        private static StrategysRegistry instance;
        private StrategysRegistry()
        {
        }
        public static StrategysRegistry Instance
        {
            get
            {
                instance = instance ?? new StrategysRegistry();
                return instance;
            }
        }
        public ISortStrategy<T> GetSortStrategy<T>(string sortSettings) where T : IComparable
        {
            switch (sortSettings)
            {
                case "quick": return new QuickSort<T>();
                case "stupid": return new StupidSort<T>();
                default: return new StupidSort<T>();
            }
        }

        public IPrintStrategy GetPrintStrategy(string printerSettings)
        {
            switch (printerSettings)
            {
                case "file": return new FilePrinter(ConfigurationManager.AppSettings["file_name"]);
                case "console": return new ConsolePrinter();
                default: return new ConsolePrinter();
            }
        }
        public ISortStrategy<T> GetSortStrategy<T>() where T : IComparable
        {
            string sortSettings = ConfigurationManager.AppSettings["sort"];
            return GetSortStrategy<T>(sortSettings);
        }

        public IPrintStrategy GetPrintStrategy()
        {
            string printerSettings = ConfigurationManager.AppSettings["printer"];
            return GetPrintStrategy(printerSettings);
        }
    }
}

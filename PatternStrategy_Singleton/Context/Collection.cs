﻿using System;
using PatternStrategy.PrintStrategy;
using PatternStrategy.PrintStrategy.Abstract;
using PatternStrategy.SortStrategy;
using PatternStrategy.SortStrategy.Abstract;

namespace PatternStrategy.Context
{
    class Collection<T> where T : IComparable
    {
        private ISortStrategy<T> sortStrategy;
        private IPrintStrategy printStrategy;
        private T[] data;
        private int size;
        private int currPos;
        public Collection(ISortStrategy<T> sortStrategy, IPrintStrategy printStrategy)
        {
            this.printStrategy = printStrategy;
            this.sortStrategy = sortStrategy;
            size = 10;
            currPos = -1;
            data = new T[size];
        }
        public void Print()
        {
            printStrategy.BeginWrite();
            for (int i = 0; i <= currPos; ++i)
                printStrategy.Write(String.Format("{0} ", data[i]));
            printStrategy.WriteLine("");
            printStrategy.EndWrite();
        }
        private void Extend()
        {
            size *= 2;
            T[] newData = new T[size];
            for (int i = 0; i < currPos; ++i)
                newData[i] = data[i];
            data = newData;
        }
        public void Add(T element)
        {
            if (++currPos >= size)
                Extend();
            data[currPos] = element;
        }
        public void Sort(bool reverse = false)
        {
            sortStrategy.Sort(data, reverse);
        }
    }
}

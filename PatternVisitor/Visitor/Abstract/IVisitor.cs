﻿using PatternVisitor.Element.Concrete;

namespace PatternVisitor.Visitor.Abstract
{
    interface IVisitor
    {
        void Visit(Rectangle rectangle);
        void Visit(Circle circle);
    }
}

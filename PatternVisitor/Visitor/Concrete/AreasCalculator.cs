﻿using PatternVisitor.Element.Concrete;
using PatternVisitor.Visitor.Abstract;

namespace PatternVisitor.Visitor.Concrete
{
    class AreasCalculator : IVisitor
    {
        public float RectanglesArea { get; private set; }
        public float CirclesArea { get; private set; }

        public void Visit(Rectangle rectangle)
        {
            RectanglesArea += rectangle.Area();
        }
        public void Visit(Circle circle)
        {
            CirclesArea += circle.Area();
        }
    }
}

﻿using System;
using System.Drawing;
using PatternVisitor.Element.Abstract;
using PatternVisitor.Visitor.Abstract;

namespace PatternVisitor.Element.Concrete
{
    class Circle : Shape
    {
        private PointF middle;
        private float radius;

        public Circle(PointF middle, float radius)
        {
            this.middle = middle;
            this.radius = radius;
        }

        public override void Visit(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override PointF Middle()
        {
            return middle;
        }
        public override float Area()
        {
            return (float) (Math.PI*radius*radius);
        }

        public override bool Intersect(Shape otherShape)
        {
            throw new System.NotImplementedException();
        }
    }
}

﻿using System;
using System.Drawing;
using PatternVisitor.Element.Abstract;
using PatternVisitor.Visitor.Abstract;

namespace PatternVisitor.Element.Concrete
{
    class Rectangle : Shape
    {
        private PointF leftTop, rightBottom;

        public Rectangle(PointF leftTop, PointF rightBottom)
        {
            this.leftTop = leftTop;
            this.rightBottom = rightBottom;
        }

        public override void Visit(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override PointF Middle()
        {
            return new PointF(Math.Abs(rightBottom.X - leftTop.X) / 2, Math.Abs(rightBottom.Y - leftTop.Y) / 2);
        }

        public override float Area()
        {
            return Math.Abs(rightBottom.X - leftTop.X) * Math.Abs(rightBottom.Y - leftTop.Y);
        }

        public override bool Intersect(Shape otherShape)
        {
            throw new NotImplementedException();
        }
    }
}

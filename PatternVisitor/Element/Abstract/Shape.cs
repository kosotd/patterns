﻿using System;
using System.Drawing;
using PatternVisitor.Visitor.Abstract;

namespace PatternVisitor.Element.Abstract
{
    abstract class Shape
    {
        public abstract void Visit(IVisitor visitor);
        public abstract PointF Middle();
        public abstract float Area();
        public virtual float Distance(Shape otherShape)
        {
            PointF otherMiddle = otherShape.Middle();
            PointF middle = Middle();
            return (float)Math.Sqrt(Math.Pow(otherMiddle.X - middle.X, 2) + Math.Pow(otherMiddle.Y - middle.Y, 2));
        }
        public abstract bool Intersect(Shape otherShape);
    }
}

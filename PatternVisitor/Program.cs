﻿using System;
using System.Collections.Generic;
using System.Drawing;
using PatternVisitor.Element.Abstract;
using PatternVisitor.Element.Concrete;
using PatternVisitor.Visitor.Concrete;
using Rectangle = PatternVisitor.Element.Concrete.Rectangle;

namespace PatternVisitor
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Shape> shapes = new List<Shape>();
            shapes.Add(new Circle(new PointF(10, 10), 10));
            shapes.Add(new Rectangle(new PointF(10, 10), new PointF(20, 20)));
            shapes.Add(new Circle(new PointF(4, 6), 4));
            shapes.Add(new Rectangle(new PointF(3, 6), new PointF(8, 8)));
            shapes.Add(new Circle(new PointF(0, 12), 7));
            shapes.Add(new Rectangle(new PointF(4, 4), new PointF(5, 5)));
            AreasCalculator calc = new AreasCalculator();
            foreach (var shape in shapes)
                shape.Visit(calc);
            Console.WriteLine("Rectangles area: {0}; circles area: {1};", calc.RectanglesArea, calc.CirclesArea);
            Console.ReadKey();
        }
    }
}
